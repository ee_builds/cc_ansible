# cc_ansible - Ansible Execution Environment

## Overview

The image built through this projects code can be downloaded at docker.io:

https://docker.io/anthonygittins/cc_ansible

To pull the latest image use the below command:

```shell
docker pull docker.io/anthonygittins/cc_ansible:latest
```

(N.B. if using podman use `podman pull`)

<br>

## Build, Tag, and Push the container image

### Build

To build the image yourself clone the project, move into the new project's directory and use the below command:

```shell
ansible-builder build --container-runtime docker -t cc_ansible:[version] -v 3
```

(N.B. the `-v 3` option allows you to see the progress of the build, this can be left off if you don't want to see the build progress)

E.g.:

```shell
ansible-builder build --container-runtime docker -t cc_ansible:v0.0.2 -v 3
```

<br>

### Tag

Before the image can be pushed to docker.io (or any other registry) the image needs to be tagged. Tag the newly created local image with tags for its new registry location:

```shell
docker image tag cc_ansible:v0.0.2 [registry_url]/[account]/cc_ansible:[version]
docker image tag cc_ansible:v0.0.2 [registry_url]/[account]/cc_ansible:latest
```

| Variable | Description | Example |
| --- | --- | --- |
| `registry` | The container registry you are storing your image in. | `docker.io`, `quay.io`, `my-company.local` |
| `account` | The account or other sub-path within the registry your image will be contained in. | `anthonygittins`, `network-images` |
| `version` | The version of the image. This can be a specific version number or `latest` to signify the latest version of the image. | `v0.0.1`, `latest` |

E.g.:

```shell
docker image tag cc_ansible:v0.0.2 docker.io/anthonygittins/cc_ansible:v0.0.1
docker image tag cc_ansible:v0.0.2 docker.io/anthonygittins/cc_ansible:latest
```

<br>

### Push

Use below command to push all tags of the defined image to your registry:

```shell
docker push -a [registry_url]/[account]/cc_ansible
```

E.g.

```shell
docker push -a docker.io/anthonygittins/cc_ansible
```

The above command will push all tags of the image to docker.io.

If you are pushing to a private registry, login to the registry before trying to push to it, e.g. using the `docker login` command.

<br>


## Requirements

| App | Version | Comment |
| --- | --- | --- |
| ansible | 2.12.0+ | |
| ansible-builder | 3.0.0+ | The format of the execution-environment.yaml file is v3 |
| Python | 3.9+ | |
| docker or podman | N/A | docker or podman must be installed and working on the system |

<br>

## Version

| Version | Base Image | Ansible |
| --- | --- | --- |
| latest | Red Hat 9.2 (UBI) | 2.14.11 |
| v0.0.2 | Red Hat 9.2 (UBI) | 2.14.11 |
